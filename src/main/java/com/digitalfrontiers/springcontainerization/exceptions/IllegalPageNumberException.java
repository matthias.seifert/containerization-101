package com.digitalfrontiers.springcontainerization.exceptions;

public class IllegalPageNumberException extends RuntimeException {
    public IllegalPageNumberException(String message) {
        super(message);
    }
}
